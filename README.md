

## What is HA?
>This module covers the basic concepts of high availability as a whole.

- [https://www.youtube.com/watch?v=iPfDQckYEcY&feature=youtu.be](https://www.youtube.com/watch?v=iPfDQckYEcY&feature=youtu.be)
- internal link [https://docs.google.com/presentation/d/1-7sJGR_ZeIagXL6D__dUFo8WpJUKYpJpHJcXoi-YjiM/edit#slide=id.g2823c3f9ca_0_9](https://docs.google.com/presentation/d/1-7sJGR_ZeIagXL6D__dUFo8WpJUKYpJpHJcXoi-YjiM/edit#slide=id.g2823c3f9ca_0_9)

## GitLab HA basics
>This module will discuss the elements of the GitLab system architecture that are relevant to high availability, followed by a quick self-assessment to reinforce what you've learned.

- [https://www.youtube.com/watch?v=r1r6lJ4DRx0&feature=youtu.be](https://www.youtube.com/watch?v=r1r6lJ4DRx0&feature=youtu.be)

- internal link [https://docs.google.com/presentation/d/1k46L7HNvwTAF7rv6gjkwuZbOHjErZLFnynM0zupZVUE/edit#slide=id.p1](https://docs.google.com/presentation/d/1k46L7HNvwTAF7rv6gjkwuZbOHjErZLFnynM0zupZVUE/edit#slide=id.p1)


## Implementing GitLab HA
>In this module, we'll take the theory of GitLab HA into practice - cloud providers and on-premise deployments will be compared and contrasted.  In the cloud, we'll examine how cloud services offerings can take some of the management burdens of high availability off your team.  For on-premise deploys, we'll discuss how GitLab's omnibus package ships with the ability to stand up a fully HA architecture "out of the box".

How you choose to implement GitLab in a highly available environment is dependant on a number of factors:

* The environment you're deploying to
* Your desired SLO/SLA for the organization
* Your experience with administering other HA solutions
* Your security requirements

The solution you choose will be based on the level of scalability and availability you require. The easiest solutions are scalable, but not necessarily highly available.  In addition, the definition of "highly available" can vary from organization or organization. 

### Environmental
The environment where GitLab will be deployed has the most impact on the final HA architecture.  An on-premise bare metal installation of GitLab will utilize much different technology than one deployed into a public or private cloud provider.  In the next sections, we will go in-depth on on-premise vs. cloud deployment of GitLab.

### Desired SLO/SLA
The balance you want to achieve when selecting an HA architecture is between complexity and the desired state - a highly available system.  The most available systems in the world often have the most complex architecture because they must plan for every eventuality an not allow even seconds of service unavailability.

### Past HA experience
If you or your team has experience with deploying HA applications in the past, you should consider those technologies and lessons learned first before taking on a new unknown technology or architecture.  For instance, if your organization already has a standard for highly-available file storage (such as an NFS appliance or highly-performant SAN) for other applications (SAP or other enterprise applications) then you should first consider using those "known" solutions before architecting something brand new to your organization _just_ for GitLab.

### Security requirements
Another factor that has an impact on the design of any system architecture are the specific security requirements of your business.  GitLab has customers in every vertical - financial services, federal governments, healthcare, etc. - so the technology can be hardened to match the requirements of any business.  But understanding those requirements and their impact on system architecture is critical.  A later section of this certification will go into security in depth.

### 👋 Scalable Architecture Examples

When an organization reaches a certain threshold it will be necessary to scale the GitLab instance. Still, true high availability may not be necessary. There are options for scaling GitLab instances relatively easily without incurring the infrastructure and maintenance costs of full high availability.

GitLab recommends that an organization begin to explore scaling when they have around 1,000 active users. At this point increasing CPU cores and memory is not recommended as there are some components that may not handle increased load well on a single host.

#### Basic Scaling
This is the simplest form of scaling and will work for the majority of cases. Backend components such as PostgreSQL, Redis and storage are offloaded to their own nodes while the remaining GitLab components all run on 2 or more application nodes.

This form of scaling also works well in a cloud environment when it is more cost-effective to deploy several small nodes rather than a single larger one.

* 1 PostgreSQL node
* 1 Redis nodes
* 2 or more GitLab application nodes (Unicorn, Workhorse, Sidekiq)
* 1 NFS/Gitaly storage server


#### Full Scaling
For very large installations it may be necessary to further split components for maximum scalability. In a fully-scaled architecture, the application node is split into separate Sidekiq and Unicorn/Workhorse nodes. One indication that this architecture is required is if Sidekiq queues begin to periodically increase in size, indicating that there is contention or not enough resources.

* 1 PostgreSQL node
* 1 Redis nodes
* 2 or more GitLab application nodes (Unicorn, Workhorse)
* 2 or more Sidekiq nodes
* 2 or more NFS/Gitaly storage servers

### 👋 High Availability Architecture Examples

When organizations require scaling and high availability the following architectures can be utilized. As the introduction section at the top of this page mentions, there is a tradeoff between cost/complexity and uptime. Be sure this complexity is absolutely required before taking the step into full high availability.

For all examples below, we recommend running Consul and Redis Sentinel on dedicated nodes. Consul is the service that provides registration and healthchecks for Postgres, enablingh Postgres HA.  Sentinel is a similar service for Redis. If Consul is running on PostgreSQL nodes or Sentinel on Redis nodes there is a potential that high resource usage by PostgreSQL or Redis could prevent communication between the other Consul and Sentinel nodes. This may lead to the other nodes believing a failure has occurred and automated failover is necessary. Isolating them from the services they monitor reduces the chances of split-brain.

The examples below do not really address high availability of NFS. Some enterprises have access to NFS appliances that manage availability. This is the best case scenario. In the future, GitLab may offer a more user-friendly solution to GitLab HA Storage.

There are many options in between each of these examples. Work with GitLab Support to understand the best starting point for your workload and adapt from there.

### Horizontal
This is the simplest form of high availability and scaling. It requires the fewest number of individual servers (virtual or physical) but does have some trade-offs and limits.

This architecture will work well for many GitLab customers. Larger customers may begin to notice certain events cause contention/high load - for example, cloning many large repositories with binary files, high API usage, a large number of enqueued Sidekiq jobs, etc. If this happens you should consider moving to a hybrid or fully distributed architecture depending on what is causing the contention.

* 2 PostgreSQL nodes
* 2 Redis nodes
* 3 Consul/Sentinel nodes
* 2 or more GitLab application nodes (Unicorn, Workhorse, Sidekiq, PGBouncer)
* 1 NFS/Gitaly server

#### Horizontal architecture diagram

![image](image1.png)

### Hybrid

In this architecture, certain components are split on dedicated nodes so high resource usage of one component does not interfere with others. In larger environments this is a good architecture to consider if you foresee or do have contention due to certain workloads.

* 2 PostgreSQL nodes
* 2 Redis nodes
* 3 Consul/Sentinel nodes
* 2 or more Sidekiq nodes
* 2 or more Web nodes (Unicorn, Workhorse, PGBouncer)
* 1 or more NFS/Gitaly servers

#### Hybrid architecture diagram

![image](image2.png)

### Fully Distributed

This architecture scales to hundreds of thousands of users and projects and is the basis of the GitLab.com architecture. While this scales well it also comes with the added complexity of many more nodes to configure, manage and monitor.

* 2 PostgreSQL nodes
* 4 or more Redis nodes (2 separate clusters for persistent and cache data)
* 3 Consul nodes
* 3 Sentinel nodes
* Multiple dedicated Sidekiq nodes (Split into real-time, best effort, ASAP, CI Pipeline and Pull Mirror sets)
* 2 or more Git nodes (Git over SSH/Git over HTTP)
* 2 or more API nodes (All requests to /api)
* 2 or more Web nodes (All other web requests)
* 2 or more NFS/Gitaly servers

### Fully Distributed architecture diagram

![image](image3.png)



## GitLab at scale case studies
>This module will discuss some real-world deployments of highly available and scalable GitLab installations.  Using these case studies, we will further explore the balance between system complexity and availability requirements.

In the last section, we discussed how GitLab HA can be implemented in a number of different scenarios.  In this section, we'll give three concrete examples of deploying GitLab at scale and how it can be achieved in different environments.

### Cloud Providers
Many cloud providers provide managed services that can save your team the administration of various parts of the GitLab stack.

### On-premise
When deploying GitLab on-premise, it may be that the managed services from a cloud provider will not be an option.  In that case, GitLab still ships "out of the box" with all the tools you need to make it highly available.

### GitLab.com
In the section on GitLab.com, we'll hear GitLab Senior Production Engineer John Northrup discuss running the largest instance of GitLab - GitLab.com!


### Cloud Providers

#### General

GitLab has many components that lend themselves very nicely to many public cloud offerings.  In this section, we will discuss in more detail the specific cloud offerings from Amazon AWS  Though the concepts for both may apply to other public cloud and even private cloud providers such as Google's GCP, Microsoft Azure, Red Hat OpenShift, IBM Private Cloud, Pivotal Cloud Foundry and other offerings.

##### Database and Caching layers

Many cloud providers offer the ability to use their tooling for databases and caching - Postgres and Redis in the case of GitLab.  These will allow you to easily create a scalable, highly available layer for each of these tiers for GitLab.  

##### File system high availability

Git itself is very I/O intensive when it comes to using the underlying file system.  As such, some cloud provided file systems that offer replication do not provide the throughput required for git.  We continue to work with these providers and work towards Gitaly HA out of the box, but until then it is still necessary for you to create a method for a highly available file system for the git repositories to be stored on.

##### Object storage

Most cloud providers provide a type of object storage solution for artifacts and other objects.  GitLab can utilize any S3-compatible object storage infrastructure to store many of the items outside of the git repositories themselves.  Items such as uploads, build artifacts, container images, backups, and other things can be stored in object storage rather than on disk.  This can reduce the overall need for disk space and replication of those items by off-loading them to cloud-native object stores.

##### Load balancing and multiple availability zones

Also native to most cloud providers are ways to provide traffic load balancing and/or shaping to 1-to-n application servers.  Once you have a solution for making the database, file storage and caching layers highly available and connectable to multiple application servers, these tools can help to distribute the load of incoming requests.

#### Amazon AWS

GitLab on AWS can leverage many of the services that are already configurable with High Availability. These services have a lot of flexibility and are able to adapt to most companies, best of all is the ability to automate both vertical and horizontal scaling.

##### Architecture Overview

##### Components

* **EC2**: GitLab will deployed on shared hardware (EC2).
* **EBS**: We will also use an EBS volume to store the Git data. S
* **S3**: We will use S3 to store backups, artifacts, LFS objects, etc. 
* **ELB**: An Elastic Load Balancer will be used to route requests to the GitLab instance. See the Amazon ELB pricing.
* **RDS**: An Amazon Relational Database Service using PostgreSQL will be used to provide a High Availability database configuration. 
* **ElastiCache**: An in-memory cache environment will be used to provide a High Availability Redis configuration. 

For more information and a walkthrough of the actual installation, see https://docs.gitlab.com/ee/install/aws/.

#### Google GCP

GitLab's own large instance of GitLab - `gitlab.com` - runs on Google's cloud  - Google Cloud Platform.  To learn more see [Installing GitLab on GKE](https://www.youtube.com/watch?v=HLNNFS8b_aw)


### On-premise

GitLab's omnibus install package ships "out of the box" with everything needed to create and maintain a highly available architecture for each component of the GitLab architecture.  In this section, we'll present a high-level overview of each.  However - configuring those components is much more detailed, and each section will link to more detailed documentation to review.

As part of this certification program, you should review and become familiar with that documentation as well.

#### Overview

##### Order-of-operations

The following components will each be made highly available separately, and as some of those components depend on the other, this is the recommended order for instantiation. 

1. [Configure the database](#database)
1. [Configure Redis](#redis)
1. [Configure NFS](#nfs)
1. [Configure the GitLab application servers](#application-servers)
1. [Configure the load balancers](#load-balancers)

#### Database

You can choose to install and manage a database server (PostgreSQL/MySQL) yourself, or you can use GitLab Omnibus packages to help. GitLab recommends PostgreSQL. This is the database that will be installed if you use the Omnibus package to manage your database.

##### Requirements

* A minimum of three database nodes Each node will run the following services:
  - `PostgreSQL` - The database itself
  - `repmgrd` - A service to monitor, and handle failover in case of a failure
  - `Consul` agent - Used for service discovery, to alert other nodes when failover occurs
- A minimum of three `Consul` server nodes
- A minimum of one `pgbouncer` service node

##### Architecture

![image](image4.png)

##### Connection flow

Each service in the package comes with a set of [default ports](https://docs.gitlab.com/omnibus/package-information/defaults.html#ports). You may need to make specific firewall rules for the connections listed below:

- Application servers connect to [PgBouncer default port](https://docs.gitlab.com/omnibus/package-information/defaults.html#pgbouncer)
- PgBouncer connects to the primary database servers [PostgreSQL default port](https://docs.gitlab.com/omnibus/package-information/defaults.html#postgresql)
- Repmgr connects to the database servers [PostgreSQL default port](https://docs.gitlab.com/omnibus/package-information/defaults.html#postgresql)
- Postgres secondaries connect to the primary database servers [PostgreSQL default port](https://docs.gitlab.com/omnibus/package-information/defaults.html#postgresql)
- Consul servers and agents connect to each others [Consul default ports](https://docs.gitlab.com/omnibus/package-information/defaults.html#consul)

##### Setup

1. [Configure the Consul nodes](https://docs.gitlab.com/ee/administration/high_availability/database.html#configuring-the-consul-nodes)
1. [Configure the Database nodes](https://docs.gitlab.com/ee/administration/high_availability/database.html#configuring-the-database-nodes)
1. [Configure the Pgbouncer node](https://docs.gitlab.com/ee/administration/high_availability/database.html#configuring-the-pgbouncer-node)

For an example configuration, see the [example configuration]() here. 

For a more detailed walkthrough, see [the  GitLab documentation](https://docs.gitlab.com/ee/administration/high_availability/database.html)

#### Redis

High Availability with Redis is possible using a **Master** x **Slave**
topology with a [Redis Sentinel](https://redis.io/topics/sentinel) service to watch and automatically
start the failover procedure.

You can choose to install and manage Redis and Sentinel yourself, use
a hosted cloud solution or you can use the one that comes bundled with
Omnibus GitLab packages.

> **Notes:**
> - Redis requires authentication for High Availability. See
>  [Redis Security](https://redis.io/topics/security) documentation for more
>  information. We recommend using a combination of a Redis password and tight
>  firewall rules to secure your Redis service.
> - You are highly encouraged to read the [Redis Sentinel][sentinel] documentation
>  before configuring Redis HA with GitLab to fully understand the topology and
>  architecture.
> - This is the documentation for the Omnibus GitLab packages. For installations
>  from source, follow the [Redis HA source installation](redis_source.md) guide.
> - Redis Sentinel daemon is bundled with Omnibus GitLab Enterprise Edition only.
>  For configuring Sentinel with the Omnibus GitLab Community Edition and
>  installations from source, read the
>  [Available configuration setups](#available-configuration-setups) section
>  below.

##### Requirements

You need at least `3` independent machines: physical, or VMs running into
distinct physical machines. It is essential that all master and slaves Redis
instances run in different machines. If you fail to provision the machines in
that specific way, any issue with the shared environment can bring your entire
setup down.

It is OK to run a Sentinel alongside of a master or slave Redis instance.
There should be no more than one Sentinel on the same machine though.

You also need to take into consideration the underlying network topology,
making sure you have redundant connectivity between Redis / Sentinel and
GitLab instances, otherwise the networks will become a single point of
failure.

For a more detailed walkthrough, see [the  GitLab documentation](https://docs.gitlab.com/ee/administration/high_availability/redis.html)
#### NFS

NFS should be configured using your corporate standards for NFS / file stoarge.  There probably is already a highly-available file system architecture in your environment, and assuming it can meet the requirements below and is highly performant, you should start by testing that solution with your GitLab application servers. 

##### Required features

**File locking**: GitLab **requires** advisory file locking, which is only
supported natively in NFS version 4. NFSv3 also supports locking as long as
Linux Kernel 2.6.5+ is used. We recommend using version 4 and do not
specifically test NFSv3.

##### Recommended options

When you define your NFS exports, we recommend you also add the following
options:

- `no_root_squash` - NFS normally changes the `root` user to `nobody`. This is
  a good security measure when NFS shares will be accessed by many different
  users. However, in this case only GitLab will use the NFS share so it
  is safe. GitLab recommends the `no_root_squash` setting because we need to
  manage file permissions automatically. Without the setting you may receive
  errors when the Omnibus package tries to alter permissions. Note that GitLab
  and other bundled components do **not** run as `root` but as non-privileged
  users. The recommendation for `no_root_squash` is to allow the Omnibus package
  to set ownership and permissions on files, as needed. In some cases where the
  `no_root_squash` option is not available, the `root` flag can achieve the same
  result.
- `sync` - Force synchronous behavior. Default is asynchronous and under certain
  circumstances it could lead to data loss if a failure occurs before data has
  synced.

For a more detailed walkthrough, see [the  GitLab documentation](https://docs.gitlab.com/ee/administration/high_availability/nfs.html)

##### Note

GitLab strongly recommends against using AWS Elastic File System (EFS). Our support team will not be able to assist on performance issues related to file system access.

#### Application Servers

Assuming you have already configured a database, Redis, and NFS, you can configure the GitLab application server(s) now. 

> **Note:** There is some additional configuration near the bottom for
  additional GitLab application servers. It's important to read and understand
  these additional steps before proceeding with GitLab installation.

1. If necessary, install the NFS client utility packages using the following
   commands:

    ```
    # Ubuntu/Debian
    apt-get install nfs-common

    # CentOS/Red Hat
    yum install nfs-utils nfs-utils-lib
    ```

1. Specify the necessary NFS shares. Mounts are specified in
   `/etc/fstab`. The exact contents of `/etc/fstab` will depend on how you chose
   to configure your NFS server. See [NFS documentation](nfs.md) for the various
   options. Here is an example snippet to add to `/etc/fstab`:

    ```
    10.1.0.1:/var/opt/gitlab/.ssh /var/opt/gitlab/.ssh nfs4 defaults,soft,rsize=1048576,wsize=1048576,noatime,nofail,lookupcache=positive 0 2
    10.1.0.1:/var/opt/gitlab/gitlab-rails/uploads /var/opt/gitlab/gitlab-rails/uploads nfs4 defaults,soft,rsize=1048576,wsize=1048576,noatime,nofail,lookupcache=positive 0 2
    10.1.0.1:/var/opt/gitlab/gitlab-rails/shared /var/opt/gitlab/gitlab-rails/shared nfs4 defaults,soft,rsize=1048576,wsize=1048576,noatime,nofail,lookupcache=positive 0 2
    10.1.0.1:/var/opt/gitlab/gitlab-ci/builds /var/opt/gitlab/gitlab-ci/builds nfs4 defaults,soft,rsize=1048576,wsize=1048576,noatime,nofail,lookupcache=positive 0 2
    10.1.0.1:/var/opt/gitlab/git-data /var/opt/gitlab/git-data nfs4 defaults,soft,rsize=1048576,wsize=1048576,noatime,nofail,lookupcache=positive 0 2
    ```

1. Create the shared directories. These may be different depending on your NFS
   mount locations.

    ```
    mkdir -p /var/opt/gitlab/.ssh /var/opt/gitlab/gitlab-rails/uploads /var/opt/gitlab/gitlab-rails/shared /var/opt/gitlab/gitlab-ci/builds /var/opt/gitlab/git-data
    ```

1. Download/install GitLab Omnibus using **steps 1 and 2** from
   [GitLab downloads](https://about.gitlab.com/downloads). Do not complete other
   steps on the download page.
1. Create/edit `/etc/gitlab/gitlab.rb` and use the following configuration.
   Be sure to change the `external_url` to match your eventual GitLab front-end
   URL. Depending your the NFS configuration, you may need to change some GitLab
   data locations. See [NFS documentation](https://docs.gitlab.com/ee/administration/high_availability/nfs.html) for `/etc/gitlab/gitlab.rb`
   configuration values for various scenarios. The example below assumes you've
   added NFS mounts in the default data locations. Additionally the UID and GIDs
   given are just examples and you should configure with your preferred values.

    ```ruby
    external_url 'https://gitlab.example.com'

    # Prevent GitLab from starting if NFS data mounts are not available
    high_availability['mountpoint'] = '/var/opt/gitlab/git-data'

    # Disable components that will not be on the GitLab application server
    roles ['application_role']

    # PostgreSQL connection details
    gitlab_rails['db_adapter'] = 'postgresql'
    gitlab_rails['db_encoding'] = 'unicode'
    gitlab_rails['db_host'] = '10.1.0.5' # IP/hostname of database server
    gitlab_rails['db_password'] = 'DB password'

    # Redis connection details
    gitlab_rails['redis_port'] = '6379'
    gitlab_rails['redis_host'] = '10.1.0.6' # IP/hostname of Redis server
    gitlab_rails['redis_password'] = 'Redis Password'
    
    # Ensure UIDs and GIDs match between servers for permissions via NFS
    user['uid'] = 9000
    user['gid'] = 9000
    web_server['uid'] = 9001
    web_server['gid'] = 9001
    registry['uid'] = 9002
    registry['gid'] = 9002
    ```

    > **Note:** To maintain uniformity of links across HA clusters, the `external_url`
    on the first application server as well as the additional application
    servers should point to the external url that users will use to access GitLab.
    In a typical HA setup, this will be the url of the load balancer which will
    route traffic to all GitLab application servers in the HA cluster.
    > 
    > **Note:** When you specify `https` in the `external_url`, as in the example
    above, GitLab assumes you have SSL certificates in `/etc/gitlab/ssl/`. If
    certificates are not present, Nginx will fail to start. See
    [Nginx documentation](http://docs.gitlab.com/omnibus/settings/nginx.html#enable-https)
    for more information.

#### First GitLab application server

As a final step, run the setup rake task **only on** the first GitLab application server.
Do not run this on additional application servers.

1. Initialize the database by running `sudo gitlab-rake gitlab:setup`.
1. Run `sudo gitlab-ctl reconfigure` to compile the configuration.

> **WARNING:** Only run this setup task on **NEW** GitLab instances because it
  will wipe any existing data.

##### Extra configuration for additional GitLab application servers

Additional GitLab servers (servers configured **after** the first GitLab server)
need some extra configuration.

1. Configure shared secrets. These values can be obtained from the primary
   GitLab server in `/etc/gitlab/gitlab-secrets.json`. Add these to
   `/etc/gitlab/gitlab.rb` **prior to** running the first `reconfigure`.

    ```ruby
    gitlab_shell['secret_token'] = 'fbfb19c355066a9afb030992231c4a363357f77345edd0f2e772359e5be59b02538e1fa6cae8f93f7d23355341cea2b93600dab6d6c3edcdced558fc6d739860'
    gitlab_rails['otp_key_base'] = 'b719fe119132c7810908bba18315259ed12888d4f5ee5430c42a776d840a396799b0a5ef0a801348c8a357f07aa72bbd58e25a84b8f247a25c72f539c7a6c5fa'
    gitlab_rails['secret_key_base'] = '6e657410d57c71b4fc3ed0d694e7842b1895a8b401d812c17fe61caf95b48a6d703cb53c112bc01ebd197a85da81b18e29682040e99b4f26594772a4a2c98c6d'
    gitlab_rails['db_key_base'] = 'bf2e47b68d6cafaef1d767e628b619365becf27571e10f196f98dc85e7771042b9203199d39aff91fcb6837c8ed83f2a912b278da50999bb11a2fbc0fba52964'
    ```

1. Run `touch /etc/gitlab/skip-auto-migrations` to prevent database migrations
   from running on upgrade. Only the primary GitLab application server should
   handle migrations.

1. **Optional** Configure host keys. Copy all contents(primary and public keys) inside `/etc/ssh/` on
   the primary application server to `/etc/ssh` on all secondary servers. This
   prevents false man-in-the-middle-attack alerts when accessing servers in your
   High Availability cluster behind a load balancer.

1. Run `sudo gitlab-ctl reconfigure` to compile the configuration.

For a more detailed walkthrough, see [the  GitLab documentation](https://docs.gitlab.com/ee/administration/high_availability/gitlab.html)

#### Load Balancers

In an active/active GitLab configuration, you will need a load balancer to route
traffic to the application servers. The specifics on which load balancer to use
or the exact configuration is beyond the scope of GitLab documentation. We hope
that if you're managing HA systems like GitLab you have a load balancer of
choice already. Some examples including HAProxy (open-source), F5 Big-IP LTM,
and Citrix Net Scaler. This documentation will outline what ports and protocols
you need to use with GitLab.

##### Basic ports

| LB Port | Backend Port | Protocol        |
| ------- | ------------ | --------------- |
| 80      | 80           | HTTP  [^1]      |
| 443     | 443          | TCP or HTTPS [^1] [^2] |
| 22      | 22           | TCP             |

##### GitLab Pages Ports

If you're using GitLab Pages with custom domain support you will need some 
additional port configurations.
GitLab Pages requires a separate virtual IP address. Configure DNS to point the
`pages_external_url` from `/etc/gitlab/gitlab.rb` at the new virtual IP address. See the
[GitLab Pages documentation](https://docs.gitlab.com/ee/administration/pages/) for more information.

| LB Port | Backend Port | Protocol |
| ------- | ------------ | -------- |
| 80      | Varies [^3]  | HTTP     |
| 443     | Varies [^3]  | TCP [^4] |

For a more detailed walkthrough, see [the  GitLab documentation](https://docs.gitlab.com/ee/administration/high_availability/load_balancer.html)



### GitLab.com

In this video, [John Northrup](https://about.gitlab.com/company/team/#northrup) from our infrastructure team discusses HA in a fireside chat on how we've configured gitlab.com's HA/Geo set up. gitlab.com supports thousands of users and our customers often ask how we've scaled this and what would be ideal for them to set up themselves.

Topics covered:
- Failover state
- Monitoring
- What portions of the GL setup are stateless and which are stateful?
- NFS Redundancies
- Sidekiq Queue Splitting
- PG Bouncer
- Redis Sentinel
- GitLab Service Discovery
- HA Proxy
- Cloud vs. on-prem

#### Video

* [Watch the video on YouTube](https://www.youtube.com/watch?v=uCU8jdYzpac)


## GitLab HA ROI
>The return on investment in creating a highly available GitLab installation can vary based on your group's requirements and business size.  This module presents simple equations to consider when weighing the costs and benefits of highly available deployments

For the return on investment of standing up a highly available GitLab architecture, you can use numbers discussed in previous sections to make a determination.

For instance, what is the RPO and RTO for your GitLab installation?  How many developers will rely on the system and what does it mean to their productive and/or your customers if there are lost minutes or hours of productivity.

A simple equation to get a rough order of magnitude on the return on investment would be:

* Number of developers (users)
* Average per hour billing rate of those users
* Total downtime or lost productivity in a significant incident

For a general example - say we are supporting 1,000 users with our GitLab instance and an average cost for those users is $65/hour.  A significant downtime or lost productivity incident of 8 hours would then cost $520,000.  This means that investing in your team's knowledge of HA and perhaps even engaging [GitLab's professional services](https://about.gitlab.com/services) team may be well worth the investment.

![image](image5.png)

